import React, { useEffect, useState } from "react";
import "./App.css";

function App() {
  const [bag, setBag] = useState(37);
  const [children1, setChildren1] = useState(0);
  const [children2, setChildren2] = useState(0);
  const [children3, setChildren3] = useState(0);

  const [end, setEnd] = useState(false);

  // useEffect(() => {
  //   console.log("constructor");
  // }, []);
  // useEffect(() => {
  //   console.log("renduruje sie za kazda zmiana");
  // });
  // useEffect(() => {
  //   console.log("pokaze sie tylko jak children 1 sie zmieni");
  // }, [children1]);
  // useEffect(() => {
  //   console.log("pokaze sie jak zmii sie children 1 lub bag");
  // }, [children1, bag]);

  function Rozdaj() {
    if (bag <= 2) {
      setEnd(true);
    } else {
      setChildren1((prev) => prev + 1);
      setChildren2((prev) => prev + 1);
      setChildren3((prev) => prev + 1);
      setBag((prev) => prev - 3);
    }
  }

  function Zeruj() {
    setChildren1(0);
    setChildren2(0);
    setChildren3(0);
    setEnd(false);
    setBag(37);
  }

  return (
    <div className="container">
      <div className="counter">
        <div className="sugarShop">
          <div>Liczba cukierków do rozdania {bag}</div>
        </div>
      </div>

      <div className="sugar-but">
        <button type="text1" onClick={Zeruj}>
          Zeruj pruchnicę
        </button>

        <button
          type="text1"
          disabled={end}
          style={{ backgroundColor: end ? "black" : "#36b03c" }}
          onClick={Rozdaj}
        >
          {end ? "Nie ma juz cukierow" : "Rozdaj pruchnicę"}
        </button>
      </div>

      <div className="childrens">
        <div className="children1">
          <div className="Kids1">Dziecko 1</div>
          <div className="EatOrDie1">Licznik: {children1} </div>
        </div>

        <div className="children2">
          <div className="Kids2">Dziecko 2</div>
          <div className="EatOrDie2">Licznik: {children2} </div>
        </div>

        <div className="children3">
          <div className="Kids3">Dziecko 3</div>
          <div className="EatOrDie3">Licznik: {children3} </div>
        </div>
      </div>

      <div className="container-sum">
        <div className="Sum">
          {end ? "Nie ma juz do cukierków do rozdania" : "Cukierki dostępne"}

          {/* {koniec && <div> UWaga ! koniec cukierków </div>} */}
        </div>
      </div>
    </div>
  );
}

export default App;
